require essioc
require mrfioc2, 2.3.1+15
require evrisland, 2.5.0+0

#-All systems will have counters for 14Hz, PMorten and DoD
epicsEnvSet("EVREVTARGS" "N0=F14Hz,E0=14,N1=PMortem,E1=40,N2=DoD,E2=42")
#-RF EVRs should have also BPulseSt, BPulseEnd, RfSt and RfPiezoSt Counters
epicsEnvSet("EVREVTARGS" "$(EVREVTARGS),N3=BPulseSt,E3=12,N4=BPulseEnd,E4=13,N5=RFSt,E5=15,N6=RfPiezoSt,E6=22")

iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=MBL-010RFC:RFS-EVR-401,PCIID=0e:00.0,EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
dbLoadRecords "evr-databuffer-ess.db"     "P=MBL-010RFC:RFS-EVR-401"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=MBL-010RFC:RFS-EVR-401,$(EVREVTARGS=)"

afterInit('iocshLoad($(mrfioc2_DIR)/evr.r.iocsh                   "P=MBL-010RFC:RFS-EVR-401, INTREF=#")')
afterInit('iocshLoad($(mrfioc2_DIR)/evrtclk.r.iocsh               "P=MBL-010RFC:RFS-EVR-401")')



# Load snippet for evrisland
iocshLoad("$(evrisland_DIR)/evrisland-base.iocsh", "P=MBL-010RFC:RFS-EVR-401")








#- Fixing IdCycle to 64bits
dbLoadRecords "cycleid-64bits.template"               "P=MBL-010RFC:RFS-EVR-401:"
#- Fixing databuffer Information
dbLoadRecords "BDest.template" "P=MBL-010RFC:RFS-EVR-401:,PV=BDest-I"
dbLoadRecords "BMod.template" "P=MBL-010RFC:RFS-EVR-401:,PV=BMod-I"
dbLoadRecords "BPresent.template" "P=MBL-010RFC:RFS-EVR-401:,PV=BPresent-I"
dbLoadRecords "BState.template" "P=MBL-010RFC:RFS-EVR-401:,PV=BState-I"
#- Configuring PVs to be archived
iocshLoad("archive-default.iocsh","P=MBL-010RFC:RFS-EVR-401:")
#- Configuring RF PVs to be archived
iocshLoad("archive-rf.iocsh","P=MBL-010RFC:RFS-EVR-401:")


#- ----------------------------------------------------------------------------
#- SDS Metadata Capture
#- ----------------------------------------------------------------------------
iocshLoad("$(PWD)/sdsCreateMetadataEVR.iocsh","PEVR=MBL-010RFC:RFS-EVR-401:,F14Hz=F14Hz")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

